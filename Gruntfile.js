module.exports = function(grunt){

    "use strict";
    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        cssc: {
            build: {
                options: {
                    consolidateViaDeclarations: true,
                    consolidateViaSelectors:    true,
                    consolidateMediaQueries:    true
                },
                files: {
                    'build/css/master.css': 'build/css/master.css'
                }
            }
        },

        cssmin: {

            min: {

                files: [
                    {
                        expand: true,     // Enable dynamic expansion.
                        cwd: 'assets/scss',      // Src matches are relative to this path.
                        src: ['**/*.scss '], // Actual pattern(s) to match.
                        dest: 'buid/css',   // Destination path prefix.
                        ext: '.min.css',   // Dest filepaths will have this extension.
                        extDot: 'first'   // Extensions in filenames begin after the first dot
                    }
                ]
            }
        },

        concat_css: {
            options: {},
            files: {
                'build/compiled.css': ['assets/css/general.css','assets/css/desktop/chrome.css'],
            }
        },

        sass: {

            dist: {
                options: {
                    sourcemap: 'none'
                },
                files: [{
                    expand: true,
                    cwd: 'assets/scss',
                    src: ['**/*.scss '],
                    dest: 'assets/css',
                    ext: '.css'
                }]
            }
        },

        watch: {
            html: {
                files: ['index.html'],
                tasks: ['htmlhint']
            },
            js: {
                files: ['assets/js/base.js'],
                tasks: ['uglify']
            },
            css: {
                files: ['assets/scss/**/*.scss'],
                tasks: ['buildcss']
            }
        },

        readFileConfig: {
            files: ['device-config.json']
        },

        multicss: {
            files: ['device-config.json']
        },



        htmlhint: {
            build: {
                options: {
                    'tag-pair': true,
// Force tags to have a closing pair
                    'tagname-lowercase': true,
// Force tags to be lowercase
                    'attr-lowercase': true,
// Force attribute names to be lowercase e.g. <div id="header"> is invalid
                    'attr-value-double-quotes': true,
// Force attributes to have double quotes rather than single
                    'doctype-first': true,
// Force the DOCTYPE declaration to come first in the document
                    'spec-char-escape': true,
// Force special characters to be escaped
                    'id-unique': true,
// Prevent using the same ID multiple times in a document
                    'head-script-disabled': true,
// Prevent script tags being loaded in the  for performance reasons
                    'style-disabled': true
// Prevent style tags. CSS should be loaded through 
                },
                src: ['index.html']
            }
        },

        uglify: {
            build: {
                files: {
                    'build/js/base.min.js': ['assets/js/base.js']
                }
            }
        }
    });

    grunt.registerTask('default',   ['sass']);

    grunt.registerTask('hello', 'say hello', function(name){
        if(!name || !name.length)
            grunt.warn('you need to provide a name.');
        console.log('hello ' + name);
    });

    grunt.registerTask('buildcss', ['sass', /*'cssc',*/ 'cssmin']);

    grunt.registerTask('sassexpand', ['sassexpand']);

    grunt.registerTask('saasCreate', 'Creating');

    grunt.registerTask('readFileConfig', 'Reading', function () {

        var mapping = grunt.file.readJSON('device-config.json');

        // Creating scss files by device-config

        grunt.file.write('assets/scss/general.scss');

        for (var folder in mapping) {
            if (mapping.hasOwnProperty(folder)) {
               // console.log(folder + " -> " + folder, device_config[folder].browsers);

                var browser = mapping[folder].browsers;
                grunt.file.write('assets/scss/'+ folder + '/main.scss');

                for (var source in browser) {
                    //console.log(browser + " -> " + browser[source], source);
                    grunt.file.write('assets/scss/'+ folder + '/'+ source +'.scss');
                }

            }
        }

      grunt.files.set({'build/compiled.css': ['assets/css/general.css','assets/css/desktop/chrome.css']});
      grunt.task.run('concat_css');

    });
};