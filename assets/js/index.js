var detector = (function(){

    var device_config;
    var platform_type;
    var platform_name = null;
    var load_main = null;
    var load_specific = null;
    var user = detect.parse(navigator.userAgent);

    var createCss = function (platform, href) {

        var ss = document.createElement("link");

        ss.type = "text/css";
        ss.rel = "stylesheet";
        ss.href = "build/css/"+ (platform ? platform +"/" : '') + href.toLowerCase() +".min.css";
        document.getElementsByTagName("head")[0].appendChild(ss);
    };

    var attachCss = function (load_main, load_specific, platform_type, platform_name) {

        if (load_main === true) {
            createCss(null,'general');
        }

        if (load_specific === "standalone") {
            createCss(platform_type, platform_name);
        }

        if (load_specific === true) {
            createCss(platform_type, 'main' );
            createCss(platform_type, platform_name);
        }
    };

    var init = function () {

        console.log(user.browser.family, user.browser.version, user.os.name);

        loadDoc("device-config.json", loadingStatus);
    };

    function detection () {

        if (navigator.userAgent.match(/Tablet|iPad/i))
        {
            console.log("Tablet.....    ");
            platform_type = 'tablet';

        } else if(navigator.userAgent.match(/IEMobile|Windows Phone|Lumia|Android|webOS|iPhone|iPod|Blackberry|PlayBook|BB10|Mobile Safari|Opera Mini|\bCrMo\/|Opera Mobi/i) )
        {
            console.log("Mobile.....    ");
            platform_type = 'mobile';

        } else {
            platform_type = 'desktop';
            load_main = device_config.desktop.main;
            load_specific = device_config.desktop.specific;

            if (device_config.desktop.specific === true || device_config.desktop.specific === "standalone" ) {
                platform_name = user.browser.family
            }

            console.log("Desktop.....    ");
        }

        attachCss(load_main, load_specific, platform_type, platform_name);
    };

    function loadDoc(url, cfunc) {
        var xhttp;

        xhttp=new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                cfunc(xhttp);
            }
        };
        xhttp.open("GET", url, true);
        xhttp.send();
    };

    function loadingStatus(xttp) {
        device_config = JSON.parse(xttp.responseText);

        var mapping = device_config;

        // Just for test:

        for (var folder in mapping) {
            if (mapping.hasOwnProperty(folder)) {
                console.log(folder + " -> " + folder, device_config[folder].browsers);

                var browser = device_config[folder].browsers;

                for (var source in browser) {
                    console.log(browser + " -> " + browser[source], source);
                }
            }
        }

        detection();
    }

    return {
        init: init
    }
})();

document.addEventListener("DOMContentLoaded", function() {
    detector.init();
});